require ‘webrick’

Server = WEBrick::HTTPServer.new :Port => 5000

#The following proc is used to customize the server operations
server.mount_proc ‘/‘ do | request, response|
    Response.body = ‘Hello, World’
end

server.start